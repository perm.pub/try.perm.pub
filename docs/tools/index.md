---
template: mailerlite.html
---

Baseprint Tools
===============

To publish a *Baseprint document succession*,
you need to create a *Baseprint document snapshot*.
Different tools are available for creating Baseprint snapshots and successions.

Subscribe to receive updates on Baseprint tools:

<div class="subscribe" markdown>
<div class="subscribe-col" markdown>
<div class="ml-embedded" data-form="QumI0q"></div>
</div>
</div><!--subscribe-->

Tools for Baseprint Snapshots
-----------------------------

### Baseprinter

[Baseprinter](../baseprinter/index.md) is an authoring tool that
uses Pandoc to *read* source files and [epijats](https://gitlab.com/perm.pub/epijats)
to *write* webpage and PDF previews.
For more details, refer to the [Baseprinter page](../baseprinter/index.md).
Authors will likely want to use Baseprinter in some form at some point in their authoring workflow.

### Pandoc

You can use any input source files supported by Pandoc to generate a Baseprint snapshot.
However, not all features of all Pandoc input formats are preserved.
For more details, see the [Pandoc page](../pandoc/index.md) in this documentation.

### Quarto

As of October 2023, Quarto has not been tested for creating Baseprint snapshots.
By the end of 2023, Castedo plans to test and document using Quarto to create Baseprint snapshots.

### Word processors

Pandoc can convert from multiple word processor file formats.
However, if you prefer word processor applications,
you may find the tools for Baseprints in 2023 lacking the user-friendliness you expect.

Tools for Baseprint Successions
-------------------------------

### Hidos

For an introduction on how to use Hidos,
refer to the [tutorial to Publish a Temporary Document Succession](../tutorial/pub_unsigned.md).
As of 2024, [Hidos](https://hidos.readthedocs.org)
is the only practical tool for creating a document succession.

### Git

You can use Git directly using the
[Document Succession Highly Manual Toolkit Manual](https://manual.perm.pub/)
to create a Baseprint document succession,
but it will be less convenient than using [Hidos](https://hidos.readthedocs.org).
