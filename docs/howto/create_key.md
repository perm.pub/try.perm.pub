How to Create an SSH Signing Key
================================

To sign a document succession, you need an SSH signing key.


## Prerequisites

Ensure that you have installed:

* Git version 2.34 or newer (run `git -v` to check)
* OpenSSH 8.8 or newer (run `ssh -V` to check)


## Steps

### Step 1: Create an SSH Signing Key

Run the following command in your terminal:

```bash
ssh-keygen -t ed25519 -C "me@example.com" -N "" -f my_signing_key
```

This command will generate two files:

- `my_signing_key` - the **private** key file
- `my_signing_key.pub` - the public key file

You can store the public key file `my_signing_key.pub` anywhere,
even in a public location.
However, **make sure that your private key file remains private**.
Anyone with access to this signing key can amend new editions to your document succession.
A common location to store SSH key files is the local directory `~/.ssh/`,
where other SSH keys are often kept.

### Step 2: Configure Git to Use Your SSH Signing Key

First, configure Git to use SSH keys for signing instead of GPG keys:

```
git config --global gpg.format ssh
```

Next, let Git know the location of your private SSH signing key. For example:

```
git config --global user.signingkey ~/.ssh/my_signing_key
```

Save the `my_signing_key.pub` file somewhere for future access.
You will use it to create a signed succession.
