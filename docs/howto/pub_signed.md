How to Publish a Signed Succession
==================================

This guide explains how to publish a
[baseprint](https://perm.pub/HKSI5NPzMFmgRlb4Vboi71OTKYo/)
in a signed document succession.
A document succession must be signed to be publicly shared for the long-term.

If you only need to create a temporary document succession for testing,
it will be easier to follow the tutorial
[Publish a Baseprint in a Temporary Document Succession](../tutorial/pub_unsigned.md).


Prerequisites
-------------

Before publishing a signed succession, ensure you have the following:

* A [GitHub](https://github.com) account
* Git version 2.34 or newer installed (run `git -v` to check)
* OpenSSH 8.8 or newer installed (run `ssh -V` to check)
* [Hidos](https://hidos.readthedocs.io) installed
* An SSH signing key
* Contents of a baseprint

### SSH Signing Key Requirement

Git must be configured to use an SSH signing key for signed commits.
Additionally, the corresponding *public* SSH signing key file must be available for use with Hidos.
Follow the instructions in
[Create an SSH Signing Key for Git](./create_key.md) to meet this requirement.

### Baseprint Contents Requirement

There are multiple ways to obtain the contents of a baseprint.
One method is described in the tutorial
[Author a Document Locally](../tutorial/author_locally.md),
as shown below:

```
echo Hello World > doc.md
baseprinter doc.md -b=baseprint
```


## Inputs

The following variables are inputs to the steps in this guide:

`<username>`

:   Your GitHub account username.

`<path-to-baseprint-contents>`

:   The path to a file directory containing the contents of a baseprint.
    Typically, this directory includes a file named `article.xml`.

`<path_to_public_signing_key>`

:   The filename of the public signing key, usually ending in `.pub`.

!!! danger

    Make sure you DO NOT use your *private* signing key when you should use your public
    signing key.


Steps
-----

### 1. Create an empty GitHub repository

Do not initialize your new repository with any files.
Start with an empty repository.

[Create a new repository](https://github.com/new){ .md-button .md-button--primary target='_blank' }

Let `<reponame>` represent the name given to this new repository.

!!! note
    This repository for publication is distinct from the repository, if any,
    used for the source files used to generate baseprint contents.

### 2. Create a local bare git repository

```
git init --bare <reponame>
cd <reponame>
```

### 3. Create a signed document succession

```bash
hidos git create --keys <path_to_public_signing_key> main
```

A new Document Succession Identifier (DSI) will be printed. Take note of this DSI,
as it is used to identify the document succession by other websites and software.

!!! note
    If you do not like the generated DSI,
    you can regenerate another by performing
    `git branch -D main` and repeating the `hidos git create` step.

### 4. Commit the baseprint contents to the document succession

```bash
hidos git commit <path-to-baseprint-contents> main 1
```

### 5. Push the document succession to GitHub

```bash
git push <url-to-github-repo>
```

Replace `<url-to-github-repo>` with either:

`https://github.com/<username>/<reponame>.git`

or

`git@github.com:<username>/<reponame>.git`

### Conclusion

You have successfully created a signed document succession and added a baseprint to it as edition 1.
Only individuals with the private signing key can amend this document succession.
Now, software and websites can retrieve this document succession using the DSI.
For example,
using the [Hidos](https://hidos.readthedocs.io/en/latest/reference/) software,
anyone can run the following command to read the document succession:

```
hidos get dsi:<your_new_dsi> --output local_copy
```


More Resources
--------------

See the [Hidos Reference](https://hidos.readthedocs.io/en/latest/reference/) for more
details on hidos commands.
