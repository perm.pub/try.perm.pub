Get Started with Baseprinter
============================

There are three ways to run Baseprinter.

### 1. GitHub Actions workflow

This is the easiest way to run Baseprinter.
Follow either the:

* [how-to guide to Run Baseprinter via GitHub](howto/minimal_github.md), or
* [tutorial to Author a Document via GitHub](../tutorial/author_via_github.md), or
* [tutorial to Author a Document via Overleaf](../tutorial/author_via_overleaf.md).

### 2. [OCI](https://en.wikipedia.org/wiki/Open_Container_Initiative) (Docker) container

If you prefer using the command line interface and want to avoid common problems
with software package installation, you can run Baseprinter from an OCI container.
This option is recommended if you are not running from Linux.
Refer to the [How-to Guide to Run Baseprinter via Container](howto/container.md).

### 3. Locally installed packages

This option is for advanced Linux users and provides the most power and flexibility.
Refer to the [How-to Guide to Run Baseprinter Locally](howto/install.md).
Running Baseprinter via a container will download and cache almost a gigabyte of
container image data.
Installing Baseprinter locally will consume much less disk space.
