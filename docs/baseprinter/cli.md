Command Line Interface
======================

Hello World Example
-------------------

```
echo Hello World > doc.md
baseprinter doc.md -b=baseprint
```

The above example generates a Baseprint document snapshot in the `baseprint`
subdirectory.
The snapshot contains a Baseprint JATS XML file named `article.xml`.


Setup
-----

To use Baseprinter from the command line, it can be run in two ways:

* as a OCI (Docker) container, or
* from locally installed packages.

For installation instructions, refer to the [Baseprinter start page](start.md).


Usage
-----

```
usage: baseprinter [-h] [-b BASEPRINT] [-o OUTDIR] [-C DIRECTORY] [--skip-pdf]
                   [-m MONITOR] [-d DEFAULTS]
                   [infiles ...]

positional arguments:
  infiles               pandoc input files

options:
  -h, --help            show this help message and exit
  -b BASEPRINT, --baseprint BASEPRINT
                        baseprint output directory
  -o OUTDIR, --outdir OUTDIR
                        HTML/PDF output directory
  -C DIRECTORY, --directory DIRECTORY
                        run from this directory
  --skip-pdf            Do not generate PDF
  -m MONITOR, --monitor MONITOR
                        paths to monitor
  -d DEFAULTS, --defaults DEFAULTS
                        pandoc default option settings
```


Inputs
------

The non-option arguments for input files (`infiles ...`) are passed through to
[`pandoc`](https://pandoc.org).
Pandoc will concatenate multiple input files.
Pandoc can convert Markdown, LaTeX, and many other source file formats.

Command Line Options
--------------------


### `-d DEFAULTS`

Options for `-d` and `--defaults` for defaults files are also passed through to `pandoc`.
For more information, see the [Defaults files section of the Pandoc User's
Guide](https://pandoc.org/MANUAL.html#defaults-files).

### `-m MONITOR`

Refer to the
[How to Auto-Refresh with Baseprinter](../baseprinter/howto/autorefresh.md)
guide.

