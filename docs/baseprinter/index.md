Baseprinter
===========

Baseprinter is an authoring tool that:

* generates [Baseprint document snapshots](https://perm.pub/HKSI5NPzMFmgRlb4Vboi71OTKYo),
* reads [Pandoc](https://pandoc.org) source formats such as LaTeX and Markdown,
* can generate HTML and PDF previews,
* can automatically regenerate outputs when source files are edited, and
* is open-source ([gitlab.com/perm.pub/baseprinter](https://gitlab.com/perm.pub/baseprinter)).

For a Command Line Interface (CLI) reference, refer to the [CLI page](cli.md).

To get started, visit the [Start page](start.md).

Implementation
--------------

Baseprinter is a wrapper around:

[Pandoc](https://pandoc.org)
:   A universal document converter.
    If you are only generating the contents of a Baseprint document snapshot,
    you can also [run Pandoc directly](../pandoc/index.md).

[Epijats](https://gitlab.com/perm.pub/epijats):
:   A Python library (with many non-Python dependencies)
    for converting Baseprint document snapshots into webpages and PDF files.

[Python watchfiles](https://pypi.org/project/watchfiles/):
:   A Python library for file watching to rerun code.

