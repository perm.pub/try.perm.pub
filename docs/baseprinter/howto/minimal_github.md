<!-- copybreak off -->

How to Run Baseprinter via GitHub
=================================

Baseprinter can be run in a GitHub Actions workflow.
Baseprinter generates
a snapshot of your document in
[Baseprint](https://perm.pub/HKSI5NPzMFmgRlb4Vboi71OTKYo/)
Document Format (BDF)
and an HTML/PDF preview with GitHub Pages.


Objective
---------

You will create a minimal 
[GitHub](https://github.com) repository that
will automatically run Baseprinter
whenever source files are updated.


Alternatives
------------

Instead of this how-to guide, you can alternatively
follow the [tutorial to author via GitHub](../../tutorial/author_via_github.md),
which demonstrates using an example LaTeX file as source.


Prerequisites
-------------

* A GitHub account
* Source file in a Pandoc compatible format, such as Markdown or LaTeX

<!-- copybreak off -->

Steps
-----

### 1. Create a repository

First, create a new repository from a repository template.
This new repository will contain:

- a GitHub Actions workflow file that automatically generates Baseprint snapshots and previews, and
- a trivial `pandocin.yaml` Pandoc defaults file.

These files will be copied from
[this Baseprinter repository template](https://github.com/castedo/baseprint-starter/).
The workflow file is located at `.github/workflows/baseprinter.yaml`.

[Create repository from the template](
https://github.com/new?template_owner=castedo&template_name=baseprint-starter
){ .md-button .md-button--primary target='_blank' }

### 2. Enable GitHub Pages

Follow [these substeps to publish to GitHub Pages](
https://docs.github.com/en/pages/getting-started-with-github-pages/configuring-a-publishing-source-for-your-github-pages-site#publishing-with-a-custom-github-actions-workflow
):

- On GitHub, navigate to your site's repository.
- Under your repository name, click **Settings**
  (it might be a tab or in a dropdown menu).
- In the **Code and automation** section of the sidebar, click **Pages**.
- Under **Build and deployment**, under **Source**, select **GitHub Actions**.

<!-- copybreak off -->

### 3. Commit the Updated `pandocin.yaml` with the New Source File

Create a new source file in any Pandoc-compatible format you prefer,
for example, a Markdown file named `doc.md`.
Replace the input file path `/dev/null` in `pandocin.yaml` with the
relative path to the source file.
Then, commit both files to the repository and update the GitHub repository.

### 4. Show the Preview URL

- Click on the **Code** tab of the repository.
- Click the gear icon to the right of **About**.
- Under the **Website** field, check **Use your GitHub Pages website**.
- Click the **Save changes** button.

### 5. Check the Status of the Preview Job (Optional)

- Click the **Actions** tab.
- Click on the workflow in progress: "Deploy Baseprint preview to GitHub Pages".
- Wait for the workflow to complete successfully.
- Click on the link `https://<username>.github.io/<reponame>/` under the "deploy" job.

### Conclusion

You now have a preview of your Baseprint document snapshot available at
`https://<username>.github.io/<reponame>/`.
This preview will be automatically updated whenever there are changes to the repository.

The Baseprint document snapshot has been saved in the `autobaseprint` branch
as a directory (git tree) named `baseprint`.


Questions or Feedback
---------------------

If you have any questions or feedback, feel free to contact [Castedo](mailto:perm.pub@castedo.com).
