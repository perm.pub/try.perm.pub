How to Use Baseprinter with LaTeX/Overleaf
==========================================

Baseprinter uses [Pandoc](https://pandoc.org) instead of a traditional LaTeX compiler to
read LaTeX source files.
Pandoc can read basic LaTeX, but it does not have all the features of traditional LaTeX compilers
(such as pdfLaTeX, LaTeX, XƎLaTeX, and LuaLaTeX).
Nonetheless,
Baseprinter can be used alongside other LaTeX compilers like those found on
[Overleaf](https://www.overleaf.com/).


Objective
---------

This guide will show you how to organize input source files so that both Baseprinter and
traditional LaTeX compilers can generate Baseprint snapshots and PDF files, respectively.


Alternative
-----------

The [tutorial to Author a Document via Overleaf](../../tutorial/author_via_overleaf.md)
provides a concrete example of using both Baseprinter
and the traditional LaTeX compiler of Overleaf to read the same source files.

!!! note
    LaTeX can also be embedded inside Markdown files. Instead of authoring LaTeX files,
    you can take a completely different approach by authoring Markdown files with embedded LaTeX.


Prerequisites
-------------

* Ability to run Baseprinter (refer to the [Start page](../start.md)).
* A directory containing your input source files.


Steps
-----

### 1. Copy the template files

Download the following files into your source file directory:

[`main.tex`](latex/examples/main.tex)
:   This file is given as input to a traditional LaTeX compiler
    (e.g., pdfLaTeX, LaTeX, XƎLaTeX, and LuaLaTeX).
    It will not be used by Baseprinter/Pandoc.
    You probably do not need to modify it.
    It only contains LaTeX markup that is needed to produce PDF previews via a traditional LaTeX compiler.
    It is a minimal wrapper that includes `document.tex`.

[`document.tex`](latex/examples/document.tex)
:   This file contains the content of the document to be used by both Pandoc (via Baseprinter) and a traditional LaTeX compiler.
    This file contains the abstract and the contents of the document.

[`pandocin.yaml`](latex/examples/pandocin.yaml)
:   This file is only used by Pandoc (via Baseprinter) and contains document metadata
    such as title, date, authors, and license, but not the abstract.

### 2. Edit `pandocin.yaml` and `document.tex`

Edit your copies of `pandocin.yaml` and `document.tex` as they contain example
placeholder text.

### 3. Create `references.bib`

Create a BibTeX file `references.bib` to contain your references.
If you do not have any references yet, just create an empty file.
If you want to use a BibTeX file with a different name, change the line
`\addbibresource{references.bib}` in `document.tex`.


Conclusion 
----------

You can now run Baseprinter inside your source directory from the command line.

!!! warning
    If you run Baseprinter from a directory different than your source
    directory, use the `-C` option so that Pandoc runs from inside the source directory.
    The other paths on the Baseprinter command line should be relative to the source directory.

