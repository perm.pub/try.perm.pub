<!-- copybreak off -->

How to Run Baseprinter via Container
====================================

Running `baseprinter` inside an [OCI container](https://opencontainers.org/),
often known as a Docker container, avoids many typical installation problems.
Containers can be run using `podman` or `docker`,
with `podman` being the recommended option.
However, replacing `podman` with `docker` will _probably_ still work.
Podman is a superior drop-in replacement to the `docker` tool.


Steps
-----

### 1. Install Podman

The first step is to install `podman` if it is not already installed.

[Get Started with Podman](https://podman.io/get-started){ .md-button .md-button--primary target='_blank' }

!!! warning "Warning for Mac users"
    Mac users will need to run
    `podman machine init --volume $HOME:$HOME` before `podman machine start`.
    On Mac, there is an intermediary virtual machine (VM) between your Mac and the container,
    and the VM needs access to your work directory.

### 2. Define a shell function

Define a shell function named `baseprinter` to run the container image
[`registry.gitlab.com/perm.pub/dock/baseprinter`](https://gitlab.com/perm.pub/dock/baseprinter/container_registry).

In the bash shell, this is achieved with the following lines:

```bash
baseprinter() {
  podman run --rm -v=$PWD:/mnt -w=/mnt registry.gitlab.com/perm.pub/dock/baseprinter baseprinter "$@"
}
```

You can manually enter these lines at the bash command line or copy them into an
initialization file (e.g., `.bashrc`).

!!! warning

    Baseprinter can ONLY access relative file paths below the current directory
    when run inside a container via the above bash function.


### 3. Test

After defining the shell function mentioned above, you can test it by simply typing the shell command
as if `baseprinter` were installed locally:

```bash
baseprinter
```

The first execution of this function will trigger a one-time download of the container image (almost 1GB!),
and then `baseprinter` will display the [command line](../cli.md) help.

If you are running with SELinux, you may need to insert `--security-opt label=disable`
after `podman run`.

NOTE: Running this shell command will share the local current working directory with the
container. Therefore, only relative paths to files under the current working directory
will work.

<!-- copybreak off -->

## Optional

When generating HTML previews of a baseprint,
it is convenient to run a local web
server that automatically reloads the preview web page upon regeneration.
For instructions, refer to [How to Auto-Refresh with Baseprinter](autorefresh.md).
