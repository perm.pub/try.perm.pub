How to Install Baseprinter
==========================

Baseprinter is tested on Linux, specifically Fedora 41, Ubuntu 24.04, and Debian Trixie.
Mac and Windows users should [run Baseprinter in a container](container.md).
Linux users should also consider [running Baseprinter in a container](container.md).

Installing Baseprinter locally on distributions other than Fedora, Ubuntu 24+, and
Debian Trixie will likely require additional effort.
If you are installing on another Linux distribution, it might be useful to see how
the Baseprinter container images are built:
[baseprinter-base](https://gitlab.com/perm.pub/dock/baseprinter-base/),
[baseprinter-core](https://gitlab.com/perm.pub/dock/baseprinter-core/), and
[baseprinter](https://gitlab.com/perm.pub/dock/baseprinter/).


## Basic Installation

The following common tools must be installed: `git`, `pip`, and `npm`.

[Installing pandoc](https://pandoc.org/installing.html#linux) is required,
 with a minimum version of 3.1.6.2.

Baseprinter is installed as a Python package:

```
python3 -m pip install baseprinter@git+https://gitlab.com/perm.pub/baseprinter.git
```

!!! warning
    Depending on your environment and preferences, you might want to use the `--user`
    option, the `--break-system-packages` option, the `sudo` command, the `venv` module,
    and/or similar alternatives.


You also need to install the following NPM package:

```
npm install -g pandoc-katex-filter
```

!!! warning
    You might need to run `npm` with the `sudo` command.


## PDF Support

The basic installation only provides HTML previews,
 which give you an idea of how the generated PDF will appear.
To enable PDF support, you need to install two additional packages:

1. [WeasyPrint Installation](https://doc.courtbouillon.org/weasyprint/stable/first_steps.html).
2. The `pdf` extra from the `baseprinter` package:

```
python3 -m pip install baseprinter[pdf]@git+https://gitlab.com/perm.pub/baseprinter.git
```
