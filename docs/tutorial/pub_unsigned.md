Publish a Temporary Document Succession
=======================================
<!-- copybreak off -->

In this tutorial, you will learn how to publish a
temporary unsigned Baseprint document succession.

If you just want to create a Baseprint document snapshot using the GitHub website,
refer to the tutorial on how to [Author a Document via GitHub](author_via_github.md).


Prerequisites
-------------

To publish an unsigned succession, ensure you have the following prerequisites:

* A [GitHub](https://github.com) account
* Git installed
* [Hidos](https://hidos.readthedocs.io) installed
* Contents of a Baseprint document snapshot

### Requirement for Contents of a Baseprint Snapshot

There are multiple ways to obtain the contents of a Baseprint snapshot.
One method is described in the tutorial
[Author a Document Locally](../tutorial/author_locally.md),
as shown below:

```
echo Hello World > doc.md
baseprinter doc.md -b=baseprint
```


## Inputs

The steps in this guide require the following input variables:

`<username>`

:   Your GitHub account username.

`<path-to-baseprint-contents>`

:   The path to the directory containing the contents of a Baseprint snapshot.
    This directory typically includes a file named `article.xml`.

<!-- copybreak off -->

Steps
-----

### 1. Create an Empty GitHub Repository

Do not initialize your new repository with any files.
Start with an empty repository.

[Create a new repository](https://github.com/new){ .md-button .md-button--primary target='_blank' }

Let `<reponame>` represent the name given to this new repository.

!!! note
    This new repository is distinct from any repository with source files, if any,
    used to generate the contents of a Baseprint snapshot.

### 2. Create a Local Bare Git Repository

```
git init --bare <reponame>
cd <reponame>
```

### 3. Create a Temporary Unsigned Document Succession

```bash
hidos git create main
```

A new Document Succession Identifier (DSI) will be printed.
Make a note of this DSI, which allows other websites and software to identify this document succession.

### 4. Commit the Baseprint Contents to the Document Succession

Assuming the contents of a Baseprint snapshot are in a directory at `../baseprint/`:

```bash
hidos git commit ../baseprint/ main 1
```

### 5. Push the Document Succession to GitHub

```bash
git push <url-to-github-repo>
```

Replace `<url-to-github-repo>` with either
`https://github.com/<username>/<reponame>.git`
or
`git@github.com:<username>/<reponame>.git`.

<!-- copybreak off -->

### Conclusion

You have successfully created an unsigned Baseprint document succession and
amended a Baseprint document snapshot to it as edition 1.
Since it is unsigned, this document succession should only be used temporarily for testing purposes.

Software and websites can now retrieve this document succession using the DSI.
For example,
by using the [Hidos](https://hidos.readthedocs.io/en/latest/reference/) software,
the following command allows anyone to read this document succession:

```
hidos get dsi:<your_new_temp_dsi> --output local_copy
```


More Resources
--------------

For long-term usage, you should use a **signed** document succession.
For more details, see [How to publish a signed succession](../howto/pub_signed.md).

For information on Hidos commands, consult the [Hidos Reference](https://hidos.readthedocs.io/en/latest/reference/).
