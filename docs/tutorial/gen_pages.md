Generate Pages from a Published Document Succession
===================================================

This tutorial will guide you through the process of downloading a document succession
based on a Document Succession Identifier (DSI) and then generating web pages from it.


Prerequisites
-------------

Make sure you have the following installed:

* Git
* Pip
* [Pandoc](https://pandoc.org/installing.html) version 3.1.6.2 or newer
* NPM package [pandoc-katex-filter](https://www.npmjs.com/package/pandoc-katex-filter)

Steps
-----

### 1. Install BaseprintPress

```
pip install --user git+https://gitlab.com/perm.pub/baseprintpress.git
```

### 2. Create a config file

Create a `baseprintpress.toml` file that includes a list of Document Succession Identifiers (DSIs).
Add the DSI of the published document succession to the file.

```
echo '[successions.HKSI5NPzMFmgRlb4Vboi71OTKYo]' > baseprintpress.toml
```

The `baseprintpress.toml` file will be read by `baseprintpress`.

### 3. Generate web pages

```
baseprintpress
```

The generated pages can be found in the newly created subdirectory `site`.
You can use a web browser to view the pages.
The file `site/index.html` will list and link
to the pages generated for the document successions listed in `baseprintpress.toml`.

### Conclusion

You have successfully "browsed" a document succession based on only its DSI.


Questions/Feedback
------------------

If you have any questions or feedback, feel free to contact [Castedo](mailto:perm.pub@castedo.com).
