Author a Document Locally
=========================
<!-- copybreak off -->

Objective
---------

In this tutorial, you will learn how to create a
[Baseprint document snapshot](https://perm.pub/HKSI5NPzMFmgRlb4Vboi71OTKYo/),
with HTML and PDF previews, locally on your computer.
A Baseprint snapshot can then extend a Baseprint document succession.


Alternatives
------------

Instead of generating a Baseprint document snapshot locally,
you can opt to use a GitHub Action workflow.
Each method offers distinct benefits.
A Baseprint snapshot generated on GitHub can be easily shared and viewed through GitHub Pages.
For more details,
refer to the tutorial on how to [Author a Document via GitHub](../tutorial/author_via_github.md).
However, generating a Baseprint snapshot locally is much faster
as it does not require a Git commit to see an updated Baseprint snapshot preview.

<!-- copybreak off -->

Prerequisites
-------------

You will need a command-line environment that is either:

- [set up to run Baseprinter in containers](../baseprinter/howto/container.md)
  (recommended)

or

- [have Baseprinter installed](../baseprinter/howto/install.md).


Steps
-----

### 1. Write a Source File

Baseprinter requires source files as input.
This how-to guide assumes you have a single source input file named `doc.md`.
However,
source files can be of any format supported by [Pandoc](https://pandoc.org),
such as LaTeX and Markdown.

Quickly create a source file by running:

```
echo Hello World > doc.md
```

### 2. Generate the Contents of a Baseprint Snapshot

Next, generate the contents of a Baseprint snapshot into a subdirectory called `baseprint`:

```
baseprinter doc.md -b=baseprint
```

The contents within the `baseprint` subdirectory can be published in a document succession,
as described in the tutorial
[Publish a Baseprint in a Temporary Document Succession](../tutorial/pub_unsigned.md).


### 3. Generate a Fast HTML Preview

To generate a fast Baseprint snapshot preview in a subdirectory named `preview`:

```
baseprinter doc.md -b=baseprint --outdir=preview --skip-pdf
```

Open the file `preview/index.html` in a web browser.
This preview will include an HTML preview,
which typically resembles a generated PDF
but on an indefinitely long single page of paper.


### 4. PDF Generation

To generate an actual PDF file, omit the `--skip-pdf` option.

```
baseprinter doc.md -b=baseprint --outdir=preview
```

<!-- copybreak off -->

## Conclusion

You have successfully generated a Baseprint document snapshot in the `baseprint` subdirectory.
You can use this Baseprint snapshot in the tutorial
[Publish a Temporary Document Succession](pub_unsigned.md).

Having a Baseprint snapshot preview automatically updated in your browser is convenient
when authoring.
To enable this functionality, consult the guide on
[How to Auto-Refresh with Baseprinter](../baseprinter/howto/autorefresh.md).
