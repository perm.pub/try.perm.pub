Author a Document via Overleaf
==============================

Objective
---------

Using the [GitHub](https://github.com) and [Overleaf](https://www.overleaf.com/) websites,
you will edit a document in LaTeX and then create a snapshot of it in
[Baseprint](https://perm.pub/HKSI5NPzMFmgRlb4Vboi71OTKYo/) Document Format (BDF).
This format is required to publish a
[document succession](../document_succession/index.md).
You will also see a Baseprint preview generated for your *Baseprint document snapshot*.


Alternatives
------------

If you prefer to author a document on your local computer,
follow the [tutorial to author a document locally](author_locally.md).


Prerequisites
-------------

* A GitHub account
* An Overleaf account with GitHub integration (paid accounts)


Steps
-----

### 1. Create a repository

First, create a new repository from a repository template.
This new repository will contain

- source text files in LaTeX format and
- a GitHub Actions workflow file that automatically generates Baseprint snapshots and
  previews.

These files will be copied from the
[Baseprinter repository template](https://github.com/castedo/basecastbot-example/).
The workflow file is located at `.github/workflows/pages-deploy.yaml`.

[Create repository from the template](
https://github.com/new?template_owner=castedo&template_name=basecastbot-example
){ .md-button .md-button--primary target='_blank' }

### 2. Enable GitHub Pages

Follow [these substeps to publish to GitHub Pages](
https://docs.github.com/en/pages/getting-started-with-github-pages/configuring-a-publishing-source-for-your-github-pages-site#publishing-with-a-custom-github-actions-workflow
):

- On GitHub, navigate to your site's repository.
- Under your repository name, click Settings. If you cannot see the "Settings" tab, select the dropdown menu, then click Settings.
- In the "Code and automation" section of the sidebar, click Pages.
- Under "Build and deployment", under "Source", select GitHub Actions.

### 3. Import from GitHub into Overleaf

- Log in to your Overleaf account
- Click "New Project" and then "Import from GitHub"
- Click "Import to Overleaf" for the GitHub repository you just created

### 4. Edit the document with Overleaf

- In Overleaf, click the "document.tex" file.
- In the Code Editor, make changes to the document text.

### 5. Push to GitHub

- Click the Overleaf "Menu" button in the top left corner.
- Under "Sync", click "GitHub".
- Click "Push Overleaf changes to GitHub".
- Enter a description of your changes and click "Commit".

### 6. Show the preview URL on GitHub

- Click on the "Code" tab of the GitHub repository `https://github.com/<username>/<reponame>`.
- Click the gear icon to the right of "About".
- In the "Website" field, enter the URL `https://<username>.github.io/<reponame>/`.
- Click the "Save changes" button.

Note that `<username>` and `<reponame>` should be replaced with your GitHub username and
repository name, respectively.

### 7. See the status of the Baseprint preview job (Optional)

- Click the "Actions" tab.
- Click on the workflow in progress to "Deploy Baseprint preview to GitHub Pages"
- Wait for the workflow to complete successfully.
- Click on the link `https://<username>.github.io/<reponame>/` under the "deploy" job.

Conclusion
----------

Congratulations!
You can now edit your source files in Overleaf and see a LaTeX preview in Overleaf.
And when you push changes from Overleaf to GitHub,
a Baseprint preview is automatically regenerated at
`https://<username>.github.io/<reponame>/`.


Questions/Feedback
------------------

If you have any questions or have feedback, feel free to contact [Castedo](mailto:perm.pub@castedo.com).
