Author a Document via GitHub
============================
<!-- copybreak off -->

Objective
---------

Using the [GitHub](https://github.com) website,
you will create a snapshot of an example document in
[Baseprint](https://perm.pub/HKSI5NPzMFmgRlb4Vboi71OTKYo/) Document Format (BDF).
This format is required to publish a
[document succession](../document_succession/index.md).
You will also see a preview generated for the example *Baseprint document snapshot*.


Alternatives
------------

* If you prefer to author a document on your local computer,
follow the [tutorial to author a document locally](author_locally.md).
* If you prefer to author a document in [Overleaf](https://www.overleaf.com/),
follow the [tutorial to author a document via Overleaf](author_via_overleaf.md).
* If you prefer to start with a minimal GitHub repository without any initial document,
follow the [How to Run Baseprinter via GitHub](../baseprinter/howto/minimal_github.md)
guide.


Prerequisites
-------------

* A GitHub account

<!-- copybreak off -->

Steps
-----

### 1. Create a repository

First, create a new repository from a repository template.
This new repository will contain:

- source text files in LaTeX format and
- a GitHub Actions workflow file that automatically generates Baseprint snapshots and
  previews.

These files will be copied from
[this Baseprinter repository template](https://github.com/castedo/baseprint-example/).
The workflow file is located at `.github/workflows/pages-deploy.yaml`.

[Create repository from the template](
https://github.com/new?template_owner=castedo&template_name=baseprint-example
){ .md-button .md-button--primary target='_blank' }

### 2. Enable GitHub Pages

Follow [these substeps to publish to GitHub Pages](
https://docs.github.com/en/pages/getting-started-with-github-pages/configuring-a-publishing-source-for-your-github-pages-site#publishing-with-a-custom-github-actions-workflow
):

- On GitHub, navigate to your site's repository.
- Under your repository name, click **Settings**
  (it might be a tab or in a dropdown menu).
- In the **Code and automation** section of the sidebar, click **Pages**.
- Under **Build and deployment**, under **Source**, select **GitHub Actions**.

<!-- copybreak off -->

### 3. Edit the document

- Click the **Code** tab.
- Click the `document.tex` file.
- Click the pencil icon above and to the right of the document text.
- Edit some text of the document.
- Click the **Commit changes...** and then **Commit changes** buttons.

### 4. Show the preview URL

- Click the **Code** tab.
- Click the gear icon to the right of **About**.
- Under the **Website** field, check "Use your GitHub Pages website".
- Click the **Save changes** button.

### 5. See the status of the preview job (Optional)

- Click the **Actions** tab.
- Click the workflow in progress: "Deploy Baseprint preview to GitHub Pages".
- Wait for the workflow to complete successfully.
- Click on the link `https://<username>.github.io/<reponame>/` under the "deploy" job.

Note that `<username>` and `<reponame>` should be replaced with your GitHub username and
repository name.

### Conclusion

You now have a preview of your Baseprint document snapshot available at
`https://<username>.github.io/<reponame>/`.
This preview will be automatically updated whenever there are changes to the repository.

The Baseprint document snapshot has been saved in the `autobaseprint` branch
as a directory (git tree) named `baseprint`.


Questions or Feedback
---------------------

If you have any questions or feedback, feel free to contact [Castedo](mailto:perm.pub@castedo.com).
