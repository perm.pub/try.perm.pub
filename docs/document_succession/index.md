Baseprint Document Successions
==============================

Data records of a
*Baseprint document succession* are replicated across multiple repositories,
such as [GitHub](https://github.com/about)
and the [Software Heritage Archive](https://www.softwareheritage.org/),
for long-term preservation.
These preserved data records, not perm.pub nor any website,
determine that an author controls the succession
to the latest edition of a published Baseprint document.

A Baseprint document succession consists of document snapshots encoded in Baseprint Document Format (PDF).
Each Baseprint snapshot can be identified by adding an edition number to a
[*Document Succession Identifier (DSI)*](https://perm.pub/1wFGhvmv8XZfPx0O5Hya2e9AyXo).

To learn about the benefits of document successions,
read ["Why Publish Document Successions"](https://perm.pub/wk1LzCaCSKkIvLAYObAvaoLNGPc).

If you have any questions, feel free to contact [Castedo](mailto:perm.pub@castedo.com).

