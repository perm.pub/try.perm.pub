About perm.pub
==============

[Perm.pub](https://perm.pub)
provides URLs that contain
[Document Succession Identifiers](https://perm.pub/1wFGhvmv8XZfPx0O5Hya2e9AyXo)
so that researchers can find
[Baseprint document successions](../document_succession/index.md).


Maintenance
------------

[Castedo Ellerman](https://castedo.com/about)
maintains this domain aspiring to the
[principles of open scholarly infrastructure](https://openscholarlyinfrastructure.org).


Infrastructure
--------------

The [perm.pub](https://perm.pub) website:
:   The source code generating the perm.pub website is at
    [gitlab.com/perm.pub/perm.pub](https://gitlab.com/perm.pub/perm.pub).
    Perm.pub consists of
    [static web pages](https://en.wikipedia.org/wiki/Static_web_page) stored in an
    [Amazon S3](https://en.wikipedia.org/wiki/Amazon_S3) bucket delivered over https via
    [Amazon CloudFront](https://en.wikipedia.org/wiki/Amazon_CloudFront).

This website (try.perm.pub):
:   This website is made with
    [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/).
    The source files generating this website are at
    [gitlab.com/perm.pub/try.perm.pub](https://gitlab.com/perm.pub/try.perm.pub)
    with the website hosted by [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).


Open-Source Software
--------------------

Noteworthy open-source projects being used:

* [Pandoc](https://pandoc.org)
* [WeasyPrint](https://weasyprint.org/)
* [Epijats](https://gitlab.com/perm.pub/epijats)
* [Hidos](https://hidos.readthedocs.io)
* [Git](https://en.wikipedia.org/wiki/Git) / [Dulwich](https://dulwich.io/)


Formats/Standards/Infrastructure
--------------------------------

* [Document Succession Identifiers](https://perm.pub/1wFGhvmv8XZfPx0O5Hya2e9AyXo)
* [Document Succession Git Layout](https://perm.pub/VGajCjaNP1Ugz58Khn1JWOEdMZ8)
* [SoftWare Hash IDs (SWHIDs)](https://swhid.org)
* [Baseprints](https://baseprints.singlesource.pub)
* [JATS](https://en.wikipedia.org/wiki/Journal_Article_Tag_Suite)
* [Software Heritage Archive](https://archive.softwareheritage.org)
* [GitHub](https://github.com)

