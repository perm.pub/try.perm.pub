Glossary
========

Baseprint Document Format (BDF)
:   A digital encoding format for the dissemination of research document snapshots
    across independent websites in both webpage and PDF formats. For more information,
    visit [baseprints.singlesource.pub](https://baseprints.singlesource.pub).

Baseprint document snapshot<br>
Baseprint snapshot
:   A document snapshot encoded in Baseprint Document Format (BDF).

Baseprint document succession
:   A succession of Baseprint document snapshots recorded in Document Succession
    Git Layout (DSGL).

Baseprinter
:   An authoring tool.
    [Documentation here](../baseprinter/index.md) and [source code here](https://gitlab.com/perm.pub/baseprinter).

BDF
:   Baseprint Document Format.

document snapshot
:   A snapshot of data encoding a document.

Document Succession Git Layout (DSGL)
:   A structured layout of Git objects in a Git commit history to represent a Baseprint
    document succession.

Document Succession Identifier (DSI)
:   An textual identifier of Baseprint document successions and their document snapshots.

DSGL
:   Document Succession Git Layout.

DSI
:   Document Succession Identifier.

snapshot
:   A record of data captured at some point in time.

