---
template: home.html
---

# _Your citable, living document. Anywhere and forever._

<div class="pilot" markdown>
<div class="pilot-questions" markdown>
<big>Do you have a document that you want to:</big>

- Preserve for the very long term?
- Always be able to update?
- Be available on multiple websites?
- Offer in both webpage and PDF formats?
- Be citable?
- Contain math?

</div>
<div class="pilot-action" markdown>
If so, become an early adopter of
[*Baseprint document successions*](document_succession/index.md).
Help advance open-source technology for improved scholarly communication.

[Sign up for the perm.pub pilot program](mailto:perm.pub@castedo.com?subject=perm.pub pilot sign-up){ .md-button .md-button--primary }
</div>
</div><!--pilot-->

<div class="cards" markdown>

<section markdown>
<span class="heading">Citable</span>

A perm.pub URL contains a
[*Document Succession Identifier (DSI)*](https://perm.pub/1wFGhvmv8XZfPx0O5Hya2e9AyXo)
that uniquely identifies your document for references. For example:

> J. Doe (2000-01-23) "My Document"<br>
> https://perm.pub/wk1LzCaCSKkIvLAYObAvaoLNGPc

</section>

<section markdown>
<span class="heading">Living</span>

Editions of your document are separately referenced by adding an edition number to the
[*Document Succession Identifier (DSI)*](https://perm.pub/1wFGhvmv8XZfPx0O5Hya2e9AyXo).
Adding `/1` identifies a 1st edition:
```
wk1LzCaCSKkIvLAYObAvaoLNGPc/1
```
When you publish a [*Baseprint document succession*](document_succession/index.md),
you control the public succession of your document editions.
</section>

<section markdown>
<span class="heading">Anywhere</span>

With compatible open-source software,
any website and device can render a
[*Baseprint document succession*](document_succession/index.md)
as PDF files and webpages that adapt to different screen widths.
A DSI is calculated from a document succession stored anywhere,
and perm.pub is not needed to generate a DSI.
</section>

<section markdown>
<span class="heading">Forever</span>

Data records of your
[*Baseprint document succession*](document_succession/index.md)
are replicated across multiple repositories,
such as [GitHub](https://github.com/about)
and the [Software Heritage Archive](https://www.softwareheritage.org/),
for long-term preservation.
These preserved data records, not perm.pub,
give you control of the succession
to the latest edition of your Baseprint document.
</section>

</div><!--cards-->

<div class="diagram">
    <img src="images/archive_federation.svg" alt="Archive Federation">
</div>


<div class="subscribe" markdown>
<div class="subscribe-col" markdown>
<div class="ml-embedded" data-form="QumI0q"></div>
</div>
</div><!--subscribe-->

## Get Started with Tutorials

* [Author a Document via GitHub](tutorial/author_via_github.md) (easiest)
* [Author a Document via Overleaf](tutorial/author_via_overleaf.md) (easy)
* [Publish a Temporary Document Succession](tutorial/pub_unsigned.md) (advanced)
* [Generate Pages for a Published Document Succession](tutorial/gen_pages.md) (most advanced)

## Example

This document is an example of a Baseprint document succession:

["Why Publish Document Successions" (dsi:wk1LzCaCSKkIvLAYObAvaoLNGPc)](https://perm.pub/wk1LzCaCSKkIvLAYObAvaoLNGPc){ target='_blank'}

