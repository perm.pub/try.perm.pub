Generating BDF Snapshots with Pandoc
====================================

[Pandoc](https://pandoc.org/) is a universal document converter
used by [Baseprinter](../baseprinter/index.md) to
convert Pandoc input source files into [Baseprint Document
Format](https://baseprints.singlesource.pub/bdf/) snapshots.

If you are not interested in the extra features of Baseprinter,
you can directly call Pandoc (version 3.1.9 and higher).
However,
you will need to use [this baseprint.yaml](baseprint.yaml) Pandoc defaults file
and [this citation-hack.csl](citation-hack.csl) Citation Style Language (CSL) file as
follows:

```
pandoc --defaults baseprint.yaml --csl citation-hack.csl ...
```

Implementation Details
----------------------

The content of [baseprint.yaml](baseprint.yaml) is:
```
--8<-- "docs/pandoc/baseprint.yaml"
```

[citation-hack.csl](citation-hack.csl) is a minimal CSL file
to ensure that inside the output BDF snapshot,
the `<xref>` JATS XML elements for citations are like this:
```
<xref alt="2" rid="ref-foo_bar" ref-type="bibr">2</xref>
```
with no additional formatting such as brackets.
Applications reading Baseprint JATS XML determine the citation style.
